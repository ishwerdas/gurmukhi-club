$(document).scroll(function(){
    $(".home").css("opacity", 1 - $(document).scrollTop() / 450);
    $(".nav-item").each(function(){
        var pageClass = $(this).attr('id').split('-')[0];
        var scrollPosition = $(document).scrollTop();
        var pagePosition = $('.'+pageClass).offset().top;
        if(scrollPosition >= pagePosition-150){
            $('.active').removeClass('active');
            $(this).addClass('active');
        }
    })
});

$(document).ready(function(){
    var currentPunjabi;
    $('.punjabi').each(function(){
        var text = $(this).text();
        var words = text.split(' ');
        var htmlString = "";
        for(word of words){
            htmlString += "<span class='clickable'>"+word+"</span> ";
        }
        $(this).html(htmlString);
    });

    $('.clickable').tooltipster({
        content: "Loading...",
        contentAsHTML: true,
        theme: 'tooltipster-shadow',
        trigger: 'click',
        maxWidth: 350,
        functionReady: function(instance, helper){
            instance.content("Loading...");
            currentPunjabi = $(helper.origin).text();
            var timeout;
            timeout = setTimeout(function(){
                search(currentPunjabi, timeout);
            }, 10);
        }
    });

    $('.nav-item').click(function(){
        var pageClass = $(this).attr('id').split('-')[0];
        $('html, body').animate({
            scrollTop: $('.'+pageClass).offset().top
        }, 600);
    });
});

var search = function(pbi, timeout){
    var pb_word = pbi;
    var apiUrl = 'http://localhost:3000/gurmukhi/v1/word/'+pb_word;
    var translationUrl = "https://translation.googleapis.com/language/translate/v2?key=AIzaSyCimyfkm7K3DUi6dJc5pat62WcX9aQbbB0";
    var pb_meaning = " ";
    var en_meaning = " ";
    var en_word = " ";
    var translationConfig = {
        'q': pb_word,
        'target': 'en',
        'source':'pa'
    };

    $.ajax({
        type: 'POST',
        url: translationUrl,
        data: translationConfig,
        success: function(data){
            en_word = data.data.translations[0].translatedText;
        },
        async:false
    });

    var oxford_appId = '6e564104';
    var oxford_appKey = 'c431e2bba5a8a26afe51421a5a868558';

    en_word = en_word.toLowerCase();
    var oxfordApi = "https://od-api.oxforddictionaries.com/api/v1/entries/en/"+en_word;
    
    $.ajax({
        url: apiUrl,
        dataType: 'json',
        success: function(data){
            pb_meaning = " "+data.definition;
        },
        async: false
    })

    $.ajax({
        url: oxfordApi,
        type: "GET",
        port: 443,
        beforeSend: function(xhr){
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader('app_id', oxford_appId);
            xhr.setRequestHeader('app_key',oxford_appKey);
        },
        success: function(data) {
            console.log('success', data);
            if(data.results){
                en_meaning = " "+data.results[0].definition;
            } else{
                en_meaning = "";
            }
        },
        fail: function(err){
            console.log('error ',err);
        },
        async: false
    });

    $('.clickable').tooltipster('content','<h2 class="pb-word">'+pb_word+'</h2><p class="en-word">'+en_word+'</p><p class="def en-def">'+en_meaning+'</p><p class="def pb-def">'+pb_meaning+'</p>');
    clearTimeout(timeout);
}