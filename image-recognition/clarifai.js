$(document).ready(function(){
    var app = new Clarifai.App(
        'C_6DUYMXZAYd9fi4gQ3aHXkuw7N6wdT8bXaAZJp8',
        'TZAMA7A2z2Si0PHq3B8Bq-EOIaWF5hfHtF5ZEtEh'
    );

    $('.search-btn').click(function(){
        $('.loader').fadeIn();
        $('.app').hide();
        $('.punjabi').html('');
        var image = $('.text').val();
        $('.result').css({
            'background-image':'url("'+image+'")'
        });
        app.models.predict(Clarifai.GENERAL_MODEL, image ).then(
            function(response) {
                var concepts = response.outputs[0].data.concepts;
                for(concept of concepts){
                    var tag = concept.name;
                    var translationUrl = "https://translation.googleapis.com/language/translate/v2?key=AIzaSyCimyfkm7K3DUi6dJc5pat62WcX9aQbbB0";

                    var translationConfig = {
                        'q': tag,
                        'target': 'pa',
                        'source':'en'
                    };

                    $.ajax({
                        type: 'POST',
                        url: translationUrl,
                        data: translationConfig,
                        success: function(data){
                            var punjabiTag = data.data.translations[0].translatedText;
                            if(punjabiTag){
                                $('.tags').append('<span class="clickable">'+punjabiTag+'</span>');
                            }
                        },
                        async:false
                    });

                }
                $('.loader').hide();
                $('.result').fadeIn(function(){
                    $('.tags').fadeIn();
                });
                $('.clickable').tooltipster({
                    content: "Loading...",
                    contentAsHTML: true,
                    theme: 'tooltipster-shadow',
                    trigger: 'click',
                    maxWidth: 350,
                    functionReady: function(instance, helper){
                        instance.content("Loading...");
                        currentPunjabi = $(helper.origin).text();
                        var timeout;
                        timeout = setTimeout(function(){
                            search(currentPunjabi, timeout);
                        }, 10);
                    }
                });
            },
            function(err) {
                console.log("An error occured", err);
            }
        );
    });



});